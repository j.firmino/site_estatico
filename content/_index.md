Este é o meu portfólio pessoal. Aqui, conto um pouco sobre minha trajetória, gostos e devaneios.

Neste site, você encontrará:

 - Minha biografia
 - Meus projetos
 - Meu currículo

Abaixo estão algumas fotos minhas para me reconhecerem nas ruas das cidades ao redor do mundo (ou do interior)...