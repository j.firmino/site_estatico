+++
title = 'Sobre mim'
date = 2024-10-12T21:18:02-03:00
draft = false
+++

Esta página contém informações sobre minha humilde pessoa.

Sou João Pedro Santos Firmino, tenho 20 anos, nascido em São Roque, interior de São Paulo, no dia 6 de maio de 2004. Meus pais se chamam Edvaldo Ramos Firmino e Luciana de Souza Santos. Tenho duas irmãs que se chamam Julia, de 18 anos, e Analu, de 9 anos. Meu maior hobbie é tocar violão, mas adoro montar quebra-cabeças e confraternizar com minha família e amigos.

Minha história começa na cidade de Alumínio, também no interior do estado, cujo nome já diz muito sobre como sua economia gira. Sempre fui muito próximo de meus avós, já que ambos moram nessa cidade e uma vez que meus pais trabalhavam durante o dia, logo, eram meus avós que cuidavam de mim e minha irmã Julia. Muito por isso, comecei a estudar na Escola Municipal Manoel Netto Filho.

Em 2009, me mudei para a cidade vizinha, Mairinque, onde meus pais moram até hoje, e continuei estudando na EMEI. Entretanto, no 3º ano do Ensino Fundamental, passei na prova do SESI, onde fiz grandes amigos que sempre estão comigo, e fiquei por lá até o 9º ano.

No meu Ensino Médio, queria me capacitar, não só técnicamente, mas para enfrentar os desafios que o mundo colocava em meu caminho. Portanto, decidi estudar na Escola Técnica Estadual Rubens de Faria e Souza, em Sorocaba, São Paulo, para ser técnico em Mecatrônica. Foi uma grande dificuldade, pois eu estava integralmente em um lugar desconhecido e onde não conhecia ninguém, longe de casa e da rotina que havia me acostumado e onde era minha zona de conforto.

Isso porém, não me fez desistir e, pelo contrário, fiz mais amizades ainda e segui aprimorando mais meu conhecimento de mundo e técnico. Entretanto, veio a pandemia, o que me fez desanimar muito da vida acadêmica, assim como meus amigos.

A pandemia trouxe muitas dificuldades à tona, mas ela com certeza foi preponderante, pois sinto que foi nesse período em que eu me aproximei ainda mais da minha família e ainda mais da minha irmã, Julia. Percebemmos que somos muito parecidos e que nunca tinhamos conversas tão profundas e sinceras sobre assuntos tão presentes em nossas vidas, apesar de sempre estarmos um do lado do outro desde de que me entendo por gente.

Voltando ao rumo da escola, meus amigos, por causa da pandemia, assumiram uma postura de que nunca conseguiriam chegar em uma faculdade de renome pública como a USP. Então eu, querendo me desafiar mais uma vez, quis provar à eles que, mesmo com nossas dificuldades, poderiamos estar num lugar alto, de maneira humilde. Assim, mesmo sem saber o que faria no futuro, decidi que iria passar na USP pelo ENEM.

Por conseguinte, meus vestibulares foram apenas o ENEM e a VUNESP, como segunda opção caso meus planos não dessem certo (sabia que seria dificílimo) e tive que decidir para o que aplicaria e escolhi a engenharia. Após o período desgastante de vestibulares, fiquei curtindo os momentos em família, percebendo a grande importância que tinham em minha vida. Acabei passando na primeira chamada na UNESP, mas não na USP, como queria. Fiquei um pouco abalado. Iniciei os estudos na UNESP de Sorocaba em Engenharia de Controle e Automação, mas logo veio a notícia em meu email: havia passado na segunda chamada do SISU na USP em Engenharia Mecatrônica.

Porém, havia um detalhe: nem tinha pensado em como iria estudar, já que o campus fica na capital, um tanto longe de minha casa. Por sorte, um grande amigo que estudou na ETEC comigo, a quem devo muito, tinha passado na FUVEST no mesmo curso que eu e estava morando perto da faculdade. Perguntei se havia mais uma vaga em seu apartamento e ele disse que sim. Se não fosse isso, não estaria onde estou hoje.

E onde estou hoje? Estou no 3º ano de graduação em Engenharia Mecatrônica na Escola Politécnica da Universidade de São Paulo, onde quis tanto estar. Agora os desafios são outros. Coninuo sem saber onde vou parar, mas sabendo que muitos estão para me apoiar, onde quer que eu vá.

A história continua...

